﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConnectionPoolingTester.DAL;

namespace ConnectionPoolingTester.BLL
{
    public class OracleClientCreator : IDataBaseClientCreator
    {
        public IDataBaseClient CreateClient()
        {
            return new OracleClient();
        }
    }
}
