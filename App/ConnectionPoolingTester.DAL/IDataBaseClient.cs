﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectionPoolingTester.DAL
{
    public interface IDataBaseClient
    {
        string ExecuteTestQuery(int sleepMin, int sleepMax, int poolMin, int poolMax);
    }
}
