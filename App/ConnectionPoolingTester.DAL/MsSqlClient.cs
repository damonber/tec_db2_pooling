﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConnectionPoolingTester.DAL
{
    public class MsSqlClient : IDataBaseClient
    {
        private string ConnectionStringName = "ConnectionPoolingTester.Properties.Settings.MsSqlServer";
        private string Query = "SELECT COUNT(1) FROM AdventureWorks.Production.Product";

        public string ExecuteTestQuery(int sleepMin, int sleepMax, int poolMin, int poolMax)
        {
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
                connectionString = string.Format(connectionString, poolMin, poolMax);

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    try
                    {
                        Thread.Sleep(new Random().Next(sleepMin, sleepMax));

                        using (SqlCommand command = new SqlCommand(Query, connection))
                        {
                            return $"Connection Id: {connection.ClientConnectionId} | Result: {command.ExecuteScalar().ToString()}";
                        }
                    }
                    catch (Exception ex)
                    {
                        return ex.Message;
                    }
                    finally
                    {
                        if (connection.State != System.Data.ConnectionState.Closed)
                        {
                            connection.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
