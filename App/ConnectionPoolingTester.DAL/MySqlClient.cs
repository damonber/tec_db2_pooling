﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace ConnectionPoolingTester.DAL
{
    public class MySqlClient : IDataBaseClient
    {
        private string ConnectionStringName = "ConnectionPoolingTester.Properties.Settings.MySqlServer";
        private string Query = "SELECT COUNT(1) FROM sakila.country";

        public string ExecuteTestQuery(int sleepMin, int sleepMax, int poolMin, int poolMax)
        {
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
                connectionString = string.Format(connectionString, poolMin, poolMax);

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();

                    try
                    {
                        Thread.Sleep(new Random().Next(sleepMin, sleepMax));

                        using (MySqlCommand command = new MySqlCommand(Query, connection))
                        {
                            return $"Connection Id: None | Result: {command.ExecuteScalar().ToString()}";
                        }
                    }
                    catch (Exception ex)
                    {
                        return ex.Message;
                    }
                    finally
                    {
                        if (connection.State != System.Data.ConnectionState.Closed)
                        {
                            connection.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
