namespace ConnectionPoolingTester
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbtnMsSql = new System.Windows.Forms.RadioButton();
            this.rbtnMySql = new System.Windows.Forms.RadioButton();
            this.rbtnOracle = new System.Windows.Forms.RadioButton();
            this.btnStart = new System.Windows.Forms.Button();
            this.txtLog = new System.Windows.Forms.RichTextBox();
            this.lblConnections = new System.Windows.Forms.Label();
            this.txtConnections = new System.Windows.Forms.TextBox();
            this.txtSleepMin = new System.Windows.Forms.TextBox();
            this.lblSleepMin = new System.Windows.Forms.Label();
            this.txtSleepMax = new System.Windows.Forms.TextBox();
            this.lblSleepMax = new System.Windows.Forms.Label();
            this.txtPoolMin = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPoolMax = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rbtnMsSql
            // 
            this.rbtnMsSql.AutoSize = true;
            this.rbtnMsSql.Location = new System.Drawing.Point(12, 12);
            this.rbtnMsSql.Name = "rbtnMsSql";
            this.rbtnMsSql.Size = new System.Drawing.Size(99, 17);
            this.rbtnMsSql.TabIndex = 0;
            this.rbtnMsSql.TabStop = true;
            this.rbtnMsSql.Text = "MS SQL Server";
            this.rbtnMsSql.UseVisualStyleBackColor = true;
            this.rbtnMsSql.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // rbtnMySql
            // 
            this.rbtnMySql.AutoSize = true;
            this.rbtnMySql.Location = new System.Drawing.Point(123, 12);
            this.rbtnMySql.Name = "rbtnMySql";
            this.rbtnMySql.Size = new System.Drawing.Size(94, 17);
            this.rbtnMySql.TabIndex = 1;
            this.rbtnMySql.TabStop = true;
            this.rbtnMySql.Text = "MySQL Server";
            this.rbtnMySql.UseVisualStyleBackColor = true;
            this.rbtnMySql.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // rbtnOracle
            // 
            this.rbtnOracle.AutoSize = true;
            this.rbtnOracle.Enabled = false;
            this.rbtnOracle.Location = new System.Drawing.Point(231, 12);
            this.rbtnOracle.Name = "rbtnOracle";
            this.rbtnOracle.Size = new System.Drawing.Size(90, 17);
            this.rbtnOracle.TabIndex = 2;
            this.rbtnOracle.TabStop = true;
            this.rbtnOracle.Text = "Oracle Server";
            this.rbtnOracle.UseVisualStyleBackColor = true;
            this.rbtnOracle.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStart.Location = new System.Drawing.Point(707, 41);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // txtLog
            // 
            this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLog.Location = new System.Drawing.Point(11, 84);
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.Size = new System.Drawing.Size(771, 398);
            this.txtLog.TabIndex = 4;
            this.txtLog.Text = "";
            this.txtLog.TextChanged += new System.EventHandler(this.txtLog_TextChanged);
            // 
            // lblConnections
            // 
            this.lblConnections.AutoSize = true;
            this.lblConnections.Location = new System.Drawing.Point(12, 51);
            this.lblConnections.Name = "lblConnections";
            this.lblConnections.Size = new System.Drawing.Size(65, 13);
            this.lblConnections.TabIndex = 5;
            this.lblConnections.Text = "Conexiones:";
            // 
            // txtConnections
            // 
            this.txtConnections.Location = new System.Drawing.Point(74, 49);
            this.txtConnections.Name = "txtConnections";
            this.txtConnections.Size = new System.Drawing.Size(37, 20);
            this.txtConnections.TabIndex = 6;
            this.txtConnections.Text = "100";
            // 
            // txtSleepMin
            // 
            this.txtSleepMin.Location = new System.Drawing.Point(186, 48);
            this.txtSleepMin.Name = "txtSleepMin";
            this.txtSleepMin.Size = new System.Drawing.Size(37, 20);
            this.txtSleepMin.TabIndex = 8;
            this.txtSleepMin.Text = "1";
            // 
            // lblSleepMin
            // 
            this.lblSleepMin.AutoSize = true;
            this.lblSleepMin.Location = new System.Drawing.Point(132, 51);
            this.lblSleepMin.Name = "lblSleepMin";
            this.lblSleepMin.Size = new System.Drawing.Size(56, 13);
            this.lblSleepMin.TabIndex = 7;
            this.lblSleepMin.Text = "Sleep min:";
            // 
            // txtSleepMax
            // 
            this.txtSleepMax.Location = new System.Drawing.Point(300, 49);
            this.txtSleepMax.Name = "txtSleepMax";
            this.txtSleepMax.Size = new System.Drawing.Size(37, 20);
            this.txtSleepMax.TabIndex = 10;
            this.txtSleepMax.Text = "5";
            // 
            // lblSleepMax
            // 
            this.lblSleepMax.AutoSize = true;
            this.lblSleepMax.Location = new System.Drawing.Point(246, 52);
            this.lblSleepMax.Name = "lblSleepMax";
            this.lblSleepMax.Size = new System.Drawing.Size(59, 13);
            this.lblSleepMax.TabIndex = 9;
            this.lblSleepMax.Text = "Sleep max:";
            // 
            // txtPoolMin
            // 
            this.txtPoolMin.Location = new System.Drawing.Point(440, 48);
            this.txtPoolMin.Name = "txtPoolMin";
            this.txtPoolMin.Size = new System.Drawing.Size(37, 20);
            this.txtPoolMin.TabIndex = 12;
            this.txtPoolMin.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(368, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Pool size min:";
            // 
            // txtPoolMax
            // 
            this.txtPoolMax.Location = new System.Drawing.Point(576, 49);
            this.txtPoolMax.Name = "txtPoolMax";
            this.txtPoolMax.Size = new System.Drawing.Size(37, 20);
            this.txtPoolMax.TabIndex = 14;
            this.txtPoolMax.Text = "20";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(504, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Pool size max:";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 494);
            this.Controls.Add(this.txtPoolMax);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPoolMin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSleepMax);
            this.Controls.Add(this.lblSleepMax);
            this.Controls.Add(this.txtSleepMin);
            this.Controls.Add(this.lblSleepMin);
            this.Controls.Add(this.txtConnections);
            this.Controls.Add(this.lblConnections);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.rbtnOracle);
            this.Controls.Add(this.rbtnMySql);
            this.Controls.Add(this.rbtnMsSql);
            this.MinimumSize = new System.Drawing.Size(810, 532);
            this.Name = "Main";
            this.Text = "ConnectionPoolingTester";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbtnMsSql;
        private System.Windows.Forms.RadioButton rbtnMySql;
        private System.Windows.Forms.RadioButton rbtnOracle;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.RichTextBox txtLog;
        private System.Windows.Forms.Label lblConnections;
        private System.Windows.Forms.TextBox txtConnections;
        private System.Windows.Forms.Label lblSleepMin;
        private System.Windows.Forms.TextBox txtSleepMin;
        private System.Windows.Forms.TextBox txtSleepMax;
        private System.Windows.Forms.Label lblSleepMax;
        private System.Windows.Forms.TextBox txtPoolMin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPoolMax;
        private System.Windows.Forms.Label label2;
    }
}

