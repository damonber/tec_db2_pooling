using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConnectionPoolingTester.BLL;
using ConnectionPoolingTester.DAL;

namespace ConnectionPoolingTester
{
    public partial class Main : Form
    {
        private IDataBaseClientCreator DBClientCreator;
        private StringBuilder logs = new StringBuilder();
        private int connections = 0;

        public Main()
        {
            InitializeComponent();
        }

        private void radioButtons_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioButton = (RadioButton)sender;

            if (radioButton.Checked)
            {
                if (radioButton == rbtnMsSql)
                {
                    DBClientCreator = new MsSqlClientCreator();
                }
                else if (radioButton == rbtnMySql)
                {
                    DBClientCreator = new MySqlClientCreator();
                }
                else if (radioButton == rbtnOracle)
                {
                    DBClientCreator = new OracleClientCreator();
                }
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            txtLog.Text = "";
            AppendText($"********************* EXECUTION STARTED *********************{Environment.NewLine}");

            try
            {
                List<Thread> threads = new List<Thread>();

                if (int.TryParse(txtConnections.Text, out connections))
                {
                    int sleepMin, sleepMax;
                    sleepMin = sleepMax = 0;
                    if (int.TryParse(txtSleepMin.Text, out sleepMin) && int.TryParse(txtSleepMax.Text, out sleepMax))
                    {
                        sleepMin *= 1000;
                        sleepMax *= 1000;

                        int poolMin, poolMax;
                        poolMin = poolMax = 0;
                        if (int.TryParse(txtPoolMin.Text, out poolMin) && int.TryParse(txtPoolMax.Text, out poolMax))
                        {
                            for (int i = 0; i < connections; i++)
                            {
                                Thread thread = new Thread(new ThreadStart(() => RunClient(i + 1, sleepMin, sleepMax, poolMin, poolMax)));
                                threads.Add(thread);
                                thread.Start();
                            }
                        }

                        else
                        {
                            AppendText("Invalid pool size");
                            AppendText($"{Environment.NewLine}********************* EXECUTION FINISHED *********************");
                        }
                    }

                    else
                    {
                        AppendText("Invalid sleep");
                        AppendText($"{Environment.NewLine}********************* EXECUTION FINISHED *********************");
                    }
                }
                else
                {
                    AppendText("Invalid number of connections");
                    AppendText($"{Environment.NewLine}********************* EXECUTION FINISHED *********************");
                }
            }
            catch (Exception ex)
            {
                AppendText(ex.Message);
            }
        }

        private void RunClient(int index, int sleepMin, int sleepMax, int poolMin, int poolMax)
        {
            try
            {
                IDataBaseClient dbclient = DBClientCreator.CreateClient();
                AppendText($"Thread {index}: {dbclient.ExecuteTestQuery(sleepMin, sleepMax, poolMin, poolMax)}");
                connections--;
                if (connections <= 0)
                {
                    AppendText($"{Environment.NewLine}********************* EXECUTION FINISHED *********************");
                }
            }
            catch (Exception ex)
            {
                AppendText($"Thread {index}: {ex.Message}");
            }
        }

        private void AppendText(string text)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(AppendText), new object[] { text });
                return;
            }

            txtLog.Text += text + Environment.NewLine;
        }

        private void txtLog_TextChanged(object sender, EventArgs e)
        {
            txtLog.SelectionStart = txtLog.Text.Length;
            txtLog.ScrollToCaret();
        }
    }
}
